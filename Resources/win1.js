var win = Ti.UI.currentWindow;

var indicator = Titanium.UI.createProgressBar({
	width : 200,
	height : 50,
	min : 0,
	max : 1,
	value : 0,
	style : Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	top : 10,
	message : 'Downloading File',
	font : {
		fontSize : 11,
		fontWeight : 'bold'
	},
	color : '#888'
});

win.add(indicator);

indicator.show();

var statusLabel = Titanium.UI.createLabel({
	text : '...',
	height : 'auto',
	width : 'auto',
	color : '#900',
	font : {
		fontSize : 16
	},
	textAlign : 'center'
});

win.add(statusLabel);

var downloadButton = Titanium.UI.createButton({
	title : 'Download/Update',
	height : 40,
	width : 200,
	top : 70
});

win.add(downloadButton);

downloadButton.addEventListener('click', function() {
	downloadDB();
});


//remote file url
var REMOTE_FILE_URL = "http://artanis.hu/iphone/dev/data/data";

//temp file
var TEMP_FILE = Ti.Filesystem.getFile(Ti.Filesystem.tempDirectory, 'temp.sql');

//sql name
var DB_NAME = 'data';

//sql file
var SQL_FILE = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory + DB_NAME + '.sql');

//first database install from Resource directory. installed only once!
var db = Ti.Database.install('/db/data.db', DB_NAME);

//list the db first!
listDB();

function downloadDB() {

	Ti.API.info("-- START DOWNLOAD PROCEDURE");

	if(TEMP_FILE.exists()) {
		
		TEMP_FILE.deleteFile();
		Ti.API.info("TEMP_FILE already exists. Now deleted.");
	}
	//set indicator to 0
	indicator.value = 0;
	
	//create httpClient
	var c = Titanium.Network.createHTTPClient();
	
	//set request timeout
	c.setTimeout(10000);
	
	//if download finished
	c.onload = function(e) {
		Ti.API.info('-- IN ONLOAD - download finished ' + this.getStatus());

		//if our remote file downloaded to temp file successfully
		if(TEMP_FILE.exists()) {

			Ti.API.info("our TEMP_FILE exists, here is it: " + TEMP_FILE.nativePath);

			// Delete old database file
			if(SQL_FILE.exists()) {
				
				SQL_FILE.deleteFile();				
				Ti.API.info("SQL_FILE (old db) already exists. Now deleted.");
				
			} else {
				
				Ti.API.info("SQL_FILE not exists, we'll create it.");
			}

			//move temporary file, to normal place
			var ok = TEMP_FILE.move(SQL_FILE.nativePath);
			

			if(ok) {
				
				//success
				Ti.API.info("-- NEW DATABASE MOVED, here: " + SQL_FILE.nativePath);
				
				//update the db
				updateDB();
				
			} else {
				
				//error
				Ti.API.info("-- ERROR WITH DATABASE MOVE");
			}

		} else {

			//if the remote file is the same
			statusLabel.text = "The remote file not changed.";
		}

	};

	c.ondatastream = function(e) {
		
		//show the process
		indicator.value = e.progress;
		
		Ti.API.info('-- ONDATASTREAM1 - PROGRESS: ' + e.progress);
	};

	c.onerror = function(e) {
		
		//error happened
		Ti.API.info('-- XHR Error ' + e.error);
		
	};
	
	//open the client
	c.open('GET', REMOTE_FILE_URL);
	
	//set the file
	c.file = TEMP_FILE;
	
	//send the data
	c.send();
}

function updateDB() {
	
	Ti.API.info('-- UPDATE DB START');

	//check the already installed db file
	var f = db.getFile();

	//If it's there, delete it and reinstall the DB
	if(f.exists() == true) {
		
		Ti.API.info('Cached DB deleted, and new installed ' + SQL_FILE.nativePath);

		f.deleteFile();
		
		Titanium.Database.install(SQL_FILE.nativePath, DB_NAME);

	} else {
		
		//Otherwise, install it for the first time (ok, we installed before, but what if other solution?:) )
		Titanium.Database.install(SQL_FILE.nativePath, DB_NAME);
		
		Ti.API.info('DB installed ');		
	}

	//delete the sql file, after installation
	if(SQL_FILE.exists()) {
		
		SQL_FILE.deleteFile();
	}

	//list db
	listDB();

}

function listDB() {
	db = Ti.Database.open(DB_NAME);

	var rows = db.execute('SELECT * FROM szotar1');
	//var rows = db.execute("SELECT name FROM sqlite_master WHERE type='table'");

	Titanium.API.info('ROW COUNT = ' + rows.getRowCount());

	var i = 0;
	
	while(rows.isValidRow()) {
		//Titanium.API.info('ID: ' + rows.field(0) );
		i++;
		rows.next();
	}
	
	rows.close();
	db.close();
	
	statusLabel.text = "DB has " + i + " rows.";
}